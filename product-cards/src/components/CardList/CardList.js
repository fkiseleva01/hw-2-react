import React, {Component} from 'react';
import Card from "../Card/Card";
import './CardList.scss'



class CardList extends Component {

    render() {
        const {cards} = this.props
        const cardList = cards.map(card =>
            <Card
                key={card.article}
                name={card.name}
                price={card.price}
                url={card.url}
                color={card.color}
                article={card.article}
                singer={card.singer}
                year={card.year}
            />
        )
        return (
            <div className={'cardList'}>
                <h1 style={{color:"white"}}>Albums</h1>
                <div className={'cardList-wrapper'}>
                    {cardList}
                </div>
            </div>
        );
    }
}


export default CardList;
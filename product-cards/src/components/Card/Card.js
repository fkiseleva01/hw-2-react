import React, {Component} from 'react';
import Button from "../Button/Button";
import Star from "../Star/Star";
import './Card.scss'
import Modal from "../Modal/Modal";
import PropTypes from 'prop-types';

class Card extends Component {
    state = {
        isClose: true,
        inStorage: false
    }

    openModal = () => {
        this.setState({
            isClose: !this.state.isClose
        })
    }
    setActive = (type) => {
        this.setState({
            isClose: type
        })
    }
    addToStorage = (type, item, name) => {
        if (type === 'cart') {
            localStorage.setItem(`cartHash${item}`, name)
            alert('Album was added to cart!')
        } else {
            localStorage.setItem(`favourHash${item}`, name)
            alert('Album was added to favour!')
            this.setState({
                inStorage: true
            })
        }
        this.setActive(true)
    }


    render() {
        const {name, price, url, color, article, year, singer} = this.props
        const modal = (
            <Modal
                header={'Adding'}
                closeButton={true}
                text={"Add this Album to cart?"}
                action={<div>
                    <button className='modal__button' onClick={() => this.addToStorage('cart', article, name)}>Yes
                    </button>
                    <button className='modal__button' onClick={() => this.setActive(true)}>No</button>
                </div>}
                hidden={this.state.isClose}
                setActive={this.setActive}
            />)


        return (
            <div className={'card'} style={{background: color}}>
                <img width={250} height={250} src={url} alt=""/>
                <h4 className={"card-title"}>"{name}"</h4>
                <p className={"card-price"}>Price: {price}</p>
                <div className={'card-desc'}>
                    <p>Singer: {singer}</p>
                    <p>Year: {year}</p>
                </div>
                <div className={'card-options'}>
                    <Star
                        handleClick={() => this.addToStorage('favour', article, name)} inStorage={this.state.inStorage}
                    />
                    <Button content={'ADD TO CART'} color={'black'} handleClick={this.openModal}/>
                </div>
                {!this.state.isClose && modal}
            </div>
        );

    }

    name
}

Card.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    year: PropTypes.string,
    singer: PropTypes.string,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,

}
Card.defaultProps = {
    year: "None",
    singer: "Unknown"
}


export default Card;